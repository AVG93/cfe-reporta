$(document).ready(function (e) {
	
	var dominio = 'http://app27.mid-interactive.com';	
	
	var ref = {'F_serv':dominio+'/services/services.php',
				'F_img':dominio+'/img/',
				'F_CommIMG':dominio+'/app/img_comments/'};
	


	window.initialize = function (a) {
		
		app.initialize();	
		
  	};
  	

	var app = {
		
		bndF : false,
		bndQ : false,
		
		Tfallas: ['No hay luz en la cuadra o colonia', 
				'No hay luz en la casa', 
				'Variación de voltaje en la cuadra o colonia', 
				'Variación de voltaje en la casa', 
				'El CFEmatico no funciona'],
				
		Tquejas: ['Por alto consumo de recibo de luz', 
				'Por mala atención en el Centro de Atención a Clientes',
				'Por extorsión o corrupción'],
		
		
		initialize: function(){
			
			app.iniSelects();
			$("[data-id='foo1']").hide();
			//app.swipe();
			
			//$('textarea').css('height', '1000px');
			
			
			$('#LOG').on('click', function(){
				
				if($('#lEmail').val() == 'admin@correo.com'){
					
					$("[data-id='foo1']").show();
					
					$.mobile.changePage("#pag-ADMINREP", { transition: "fade" });
					app.iniAdmin();
				}
				else{
					$.mobile.changePage("#pag-Dash", { transition: "fade" });
				}				
				
				
				console.log('GO DASH');
			});
			
			$('#REG').on('click', function(){
				
				$.mobile.changePage("#pag-REG", { transition: "fade" });
				console.log('GO REG');
			});
			
			$('#iREG').on('click', function(){
				
				$.mobile.changePage("#pag-LOG", { transition: "fade" });
				console.log('GO LOG');
			});
						
			
			
			$('#ICON-REP').on('click', function(){
				console.log('ICON 1');
				//$.mobile.changePage("#popupMenu", { transition: "slidedown" });
				$('#popupMenu').popup();
                    $('#popupMenu').popup("open");
			});
			
			$('#FLL').on('click', function(){
				
				$('#select-tipoQ').hide();
				$('#select-tipoF').show();
				
				if(!$('#publish').hasClass('ui-disabled'))
				{
					$('#publish').addClass('ui-disabled');
				}
				
				
				$.mobile.changePage("#pag-REP", { transition: "fade" });
				console.log('GO FLL');
			});
			
			$('#QJA').on('click', function(){
				
				$('#select-tipoF').hide();
				$('#select-tipoQ').show();
				
				if(!$('#publish').hasClass('ui-disabled'))
				{
					$('#publish').addClass('ui-disabled');
				}

				
				$.mobile.changePage("#pag-REP", { transition: "fade" });
				console.log('GO QJA');
			});
			
						
			
			$('#ICON-REC').on('click', function(){
				console.log('ICON 2');
				$.mobile.changePage("#pag-REC", { transition: "fade" });
			});
		
			
			
			$('#ICON-CON').on('click', function(){
				console.log('ICON 3');
				
				Map_UBK = new ggMaps('map_canvas');		
				DB.s_AllStores(Map_UBK, 1);	
				app.bndUBK = true;
				
				
				
				$.mobile.changePage("#pag-CON", { transition: "fade" });
				//$('#con-tabs').tabs({ selected: 0 });
			});
			
		
			
			$('#ICON-AJU').on('click', function(){
				console.log('ICON 4');
				$.mobile.changePage("#pag-AJU", { transition: "fade" });
			});
		
		
		
		
			$('.REP').on('click', function(){
				
				$.mobile.changePage("#pag-ADMINREP", { transition: "fade" });				
				
				
				console.log('GO ADMIN REP');
			});
			
			$('.CUA').on('click', function(){
				
				$.mobile.changePage("#pag-ADMINCUA", { transition: "fade" });				
				
				
				console.log('GO ADMIN CUA');
			});
			
			$('.LOG').on('click', function(){
				
				$.mobile.changePage("#pag-ADMINLOG", { transition: "fade" });				
				
				
				console.log('GO ADMIN LOG');
			});
			
		
		
		
		},
		
		iniSelects: function(){
			
			var slct = $('#select-tipoF').children();	
				
			slct.empty();				
			slct.append('<option id="tipoRep" value="choose-one" data-placeholder="true">Tipo de Falla</option>');				
			$.each(app.Tfallas, function(index, value){
				slct.append('<option value="'+index+'">'+value+'</option>');					
			});
			
			$(slct).change(function() {
				if(this.value != -1){
					$('#publish').removeClass('ui-disabled');
				}
			});
			
			
			
			
			slct = $('#select-tipoQ').children();				
					
			slct.empty();				
			slct.append('<option id="tipoRep" value="choose-one" data-placeholder="true">Tipo de Queja</option>');				
			$.each(app.Tquejas, function(index, value){
				slct.append('<option value="'+index+'">'+value+'</option>');					
			});
			
			$(slct).change(function() {
				if(this.value != -1){
					$('#publish').removeClass('ui-disabled');
				}
			});
			
		}, 
		
		
		iniAdmin: function(){
			
			app.fullRep();
			app.fullCua();
			app.fullLog();
			
		},
		
		fullRep: function(){
			var val = 0, tipo = 0, msg = '';
			var classs = ['falla', 'queja'];
			
			for(var i=0; i<10; i++){
				val = Math.floor((Math.random() * 5) + 1);
				val = val -1;
				tipo = Math.floor((Math.random() * 10) + 1);
				
				if(tipo > 5) tipo = 1;
				else tipo = 0;
				
				
				if(tipo == 0){
					//if(val > app.Tfallas.length-1) val = (val - app.Tfallas.length);
					
					msg = app.Tfallas[val];
				}
				else{
					if(val > app.Tquejas.length-1) val = (val - app.Tquejas.length);
					
					msg = app.Tquejas[val];
					
				}
				//console.log(i + ' | ' +tipo + ' - ' + val);
				
				$('#rep-list').append('<li data-icon="false" data-iconpos="left">' +
    '<a href="#" class="add-container '+classs[tipo]+'">' +
    	'<label class="add-container" data-corners="false">' +
    		'<input type="checkbox" value="true" />' +
    		'<label class="lbl_add-container">' +
	    		'<h3>'+msg+'</h3>' +
    			'<p>Santa Teresa #108 Col. San Isidro, San Nicolas de los Garza, Nuevo León.</p></div>' +
    		'</label>' +
    	'</label>' +
    '</a>' +
    '</li>');
				
				/*$('#rep-list').append('<li data-icon="false" id="rep-'+i+'" data-role="popup"><a class="GoTo '+classs[tipo]+'" href="#"> <input type="checkbox" value="true" /> <h2>'+msg+'</h2>'+
									'<p>Santa Teresa #108 Col. San Isidro, San Nicolas de los Garza, Nuevo León.</p>  </a> </li>'
									);*/
									
									$("[type=checkbox]").checkboxradio();
				
			}
			
			$('#rep-list').listview().listview('refresh');
			
		},
		
		fullCua: function(){
			
			var iniRef = 1654;
			var rndmRep = 0, rndmCua = 0, rndmDate = 0;
			
			for(var i=0; i<10; i++){
				rndmCua = Math.floor((Math.random() * 999) + 1);
				
				
				var elmnt = '<li id="cua-'+i+'"><a class="GoTo" href="#"> <h2>Cuadrilla '+ parseInt(iniRef- i)+'</h2>'+ 
        							'<p>Zona '+ rndmCua +'</p> </a></li>';
				
				if(i<4){
					
					var elmnt_2 = '<li id="Scua-'+i+'"><a class="GoTo" href="#"> <h2>Cuadrilla '+ parseInt(iniRef- i)+'</h2>'+ 
        							'<p>Zona '+ rndmCua +'</p> </a></li>';
					
					$('#Sendcua-list').prepend($(elmnt_2)
        							.on('click', function(){
        								
        								var idd = $(this).attr('id');
        								
        								$('#cua-filter').val('');
        								$('#cua-filter').val($('#' + idd + ' h2' ).text());
        								$('#cua-filter').focus();
        								
        								$("#Sendcua-list").listview().listview("refresh");
        								console.log($('#' + idd + ' h2' ).text());
        							}));					
				}
				
				$('#cua-list').prepend(elmnt);				
			}
			
			$("#Sendcua-list").listview().listview("refresh");
			$('#cua-list').listview().listview('refresh');
		},
				
		fullLog: function(){
			
			var iniRef = 1654;
			var rndmRep = 0, rndmCua = 0, rndmDate = 0;
			
			var rDate = {DD: 12, MM:09, hh:10, mm:00};
			/*<li id="123"><a class="GoTo" href="#"> <h2>ref-0001</h2> <h3><span class="ui-li-aside">
        							<strong>12/09/2014 10:25</strong></span></h3>
        							<p>Se le Asignio el Reporte 168541 a la cuadrilla 168</p> </a>
        							</li>*/
        	for(var i=0; i<10; i++){
				rndmRep = Math.floor((Math.random() * 999999) + 1);
				rndmCua = Math.floor((Math.random() * 999) + 1);
				
				rDate = app.rndmDate(rDate, 720);
				
				$('#log-list').prepend('<li id="log-'+i+'" data-icon="false"><a class="GoTo" href="#"> <h2>ref-'+ parseInt(iniRef + i)+'</h2> <h3><span class="ui-li-aside">'+
        							'<strong>'+rDate.DD+'/'+rDate.MM+'/2014 '+rDate.hh+':'+rDate.mm+'</strong></span></h3>'+
        							'<p>Se le Asignio el Reporte '+rndmRep+' a la cuadrilla '+rndmCua+'</p> </a>'+
        							'</li>');
				
			}
			
			$('#log-list').listview().listview('refresh');
		},
		
		rndmDate: function(objDate, range_mm){
			//var DD=12, MM=19, hh=10, mm=25;
			if(!range_mm)range_mm = 120;
			rndmDate = Math.floor((Math.random() * range_mm) + 1);
			//console.log( rndmDate );
			
			objDate.mm += rndmDate;
			var c = 0;
			if(objDate.mm /60 > 1){
				c = parseInt(objDate.mm /60);
				objDate.mm = parseInt(objDate.mm - (c*60));
				
				objDate.hh += parseInt(c);
			}		
			
			if(objDate.hh / 24 > 1){				
				c = parseInt(objDate.hh /24);
				objDate.hh = parseInt(objDate.hh - (c*24));
				
				objDate.DD += parseInt(c); 				
			}
			
			if(objDate.DD / 31 > 1){				
				c = parseInt(objDate.DD /31);
				objDate.hh = parseInt(objDate.DD - (c*31));
				
				objDate.MM += parseInt(c); 				
			}
			
			if(objDate.MM / 12 > 1){				
				c = parseInt(objDate.MM /12);
				objDate.MM = parseInt(objDate.MM - (c*12));								
			}
			
			if(objDate.mm < 10)objDate.mm = '0' + objDate.mm; 
			
			return objDate;
			
		},
		
		swipe: function(){
			
			var page = "#" + $( this ).attr( "id" );
	        // Get the filename of the next page that we stored in the data-next attribute
	        //next = $( this ).jqmData( "next" ),
	        // Get the filename of the previous page that we stored in the data-prev attribute
	        //prev = $( this ).jqmData( "prev" );
		    // Check if we did set the data-next attribute
		    if ( true ) {
		    	console.log('NNEXXXT _1');
		        // Prefetch the next page
		        //$.mobile.loadPage( next + ".html" );
		        // Navigate to next page on swipe left
		        $( document ).on( "swipeleft", page, function() {
		            //$.mobile.changePage( next + ".html", { transition: "slide" });
		            console.log('NNEXXXT');
		            $.mobile.changePage( "#pag-AJU", { transition: "slide" });
		        });
		        // Navigate to next page when the "next" button is clicked
		        $( ".next").on( "click", function() {
		            //$.mobile.changePage( next + ".html", { transition: "slide" } );
		            console.log('NNEXXXT');
		            $.mobile.changePage( "#pag-AJU", { transition: "slide" });
		        });
		    }
		    // Disable the "next" button if there is no next page
		    else {
		        $( ".control .next", page ).addClass( "ui-disabled" );
		    }
		    // The same for the previous page (we set data-dom-cache="true" so there is no need to prefetch)
		    /*if ( prev ) {
		        $( document ).on( "swiperight", page, function() {
		            $.mobile.changePage( prev + ".html", { transition: "slide", reverse: true } );
		        });
		        $( ".control .prev", page ).on( "click", function() {
		            $.mobile.changePage( prev + ".html", { transition: "slide", reverse: true } );
		        });
		    }
		    else {
		        $( ".control .prev", page ).addClass( "ui-disabled" );
		    }*/
		 
		}
		
		
		
	};    

	var ggMaps = function(idElement){
  		
  		this.Id_Element = idElement;  		

		this.directionsDisplay = null;
		
		this.Active = false;

		this.directionsService = new google.maps.DirectionsService();		

		this.map = null;

		this.defZoom = 16;

		this.markers = [];

		this.RestCoors = null;

		this.MiUbica = false;		

		this.iconURLPrefix = 'http://maps.google.com/mapfiles/ms/icons/';

		this.icons = [
		  ref.F_img + '7-marker-D.png',	      
	      ref.F_img + 'cfe_Marker.png',
	      this.iconURLPrefix + 'red-dot.png',
	      this.iconURLPrefix + 'green-dot.png',
	      this.iconURLPrefix + 'blue-dot.png',
	      this.iconURLPrefix + 'orange-dot.png',
	      this.iconURLPrefix + 'purple-dot.png',
	      this.iconURLPrefix + 'pink-dot.png',      
	      this.iconURLPrefix + 'yellow-dot.png'
	    ];

	    this.styles = [
		  {
		    stylers: [
		      { hue: "#00ffe6" },
		      { saturation: -20 }
		    ]
		  },{
		    featureType: "road",
		    elementType: "geometry",
		    stylers: [
		      { lightness: 100 },
		      { visibility: "simplified" }
		    ]
		  },{
		    featureType: "road",
		    elementType: "labels",
		    stylers: [
		      { visibility: "off" }
		    ]
		  }
		];
		
		this.BestSuc = null;
		
		this.LASTDATA = null;


		this.initialize= function (DATAMARKERS, nICON, InfoBox) {
			
			this.Active = true;
			this.LASTDATA = DATAMARKERS;
			
			this.get_MapHeight();
			
			var THIS = this;
			
			
			
			this.directionsDisplay = new google.maps.DirectionsRenderer();
			//directionsDisplay.setMap(null);
			this.directionsDisplay.setOptions( { suppressMarkers: true } );
			
			var ownPos = null;
			
			if(navigator.geolocation) {
	    		navigator.geolocation.getCurrentPosition(function(position) {
	      			ownPos = new google.maps.LatLng(position.coords.latitude,
	                                       position.coords.longitude);
	                
	                MiPos = new google.maps.LatLng(position.coords.latitude,
	                                       position.coords.longitude);
	                //console.log(MiPos.lat() + ' !?? ' );
	                
	                var map_canvas = document.getElementById(THIS.Id_Element);

			        var map_options = {
			          center: ownPos,
			          zoom: THIS.defZoom,
			          disableDefaultUI: true,
			          streetViewControl: true,
			          mapTypeId: google.maps.MapTypeId.ROADMAP
			        };
		
			        THIS.map = new google.maps.Map(map_canvas, map_options);
		
			        THIS.directionsDisplay.setMap(THIS.map);
		
		
			        var homeControlDiv = document.createElement('div');
			  		var homeControl = new THIS.HomeControl(homeControlDiv, THIS.map, THIS);
			  		homeControlDiv.index = 1;
			  		THIS.map.controls[google.maps.ControlPosition.TOP_RIGHT].push(homeControlDiv);
			  		

			  		if(DATAMARKERS != null){
			  			
			  			var bndADD = false;
			  			var itemsNames = [];
			  			
			  			/* - GET ITEMS LIST - */
			  			
				  		$.each(DATAMARKERS, function (index, value) {
	        				//<li><img src="img/1.png" alt="Landscape 5" class="enfocada"></li>
	        				itemsNames.push(value.nameProduct);
	        				if(DATAMARKERS.length > index+1){
	        					if(value.idTienda != DATAMARKERS[index+1].idTienda){
	        						//console.log(value.idTienda + ' == ' + 'UJU');
	        						bndADD = true;
	        					}	        					
	        				}
	        				else{
	        					//console.log(value.idTienda + ' == ' + 'Last_UJU');
	        					bndADD = true;
	        				}
	        				
	        				if(bndADD){
	        					bndADD = false;
	        					//console.log('AAAa.... ' + value.idTienda + ' - - ' + value.name);
	        					if(InfoBox){
	        						var infoBoxFragmentData = app.getProductListNamesFragment(itemsNames, value.idTienda);
	        						console.log('AAAa.... ' + infoBoxFragmentData.itemsLeft);
	        						THIS.addMarker(new google.maps.LatLng(value.xx, value.yy), '7-Eleven', nICON, true, value.idTienda, value.name, infoBoxFragmentData.frag, infoBoxFragmentData.itemsLeft);	        						
	        					}
	        					else{
	        						THIS.addMarker(new google.maps.LatLng(value.xx, value.yy), '7-Eleven', nICON, true, value.idTienda, value.name, null);
	        					}
        						
        						itemsNames = [];        						
	        				}
	        			});
	        			if(InfoBox) THIS.getNearSuc(DATAMARKERS, true);
	        			app.idTienda_ItemsLeft = [];
        			}
	                                   			
	    	}, function() {
	      		handleNoGeolocation(true);      		

	    	});
  			} else {
	    			// Browser doesn't support Geolocation
	    		handleNoGeolocation(false);
	  		}	        
  		};


		this.get_MapHeight= function(){

			$('#'+this.Id_Element).css('height', (($.mobile.getScreenHeight() - $('#FFTR').height() ) - 100)/*-$('#' + this.Id_Element + '-data').height()*/);
			//$('#'+this.Id_Element).css('height', '550px' );
			
			//console.log('5AANNNN === '+$.mobile.getScreenHeight() + ' - ' + $('#FFTR').height());
			
		};
		

		this.HomeControl= function(controlDiv, map, _THIS) {
			
			var THIS = _THIS;

	  		controlDiv.style.padding = '5px';

		  	// Set CSS for the control border
		  	var controlUI = document.createElement('div');
		  	controlDiv.appendChild(controlUI);

		  	// Set CSS for the control interior
		  	var controlText = document.createElement('div');
		  	/*controlText.style.fontFamily = 'Arial,sans-serif';
		  	controlText.style.fontSize = '12px';
		  	controlText.style.paddingLeft = '4px';
		  	controlText.style.paddingRight = '4px';*/
		  	//controlText.innerHTML = '<b>Mi Ubicación</b>';
		 controlText.style.height='32px';
		 controlText.style.width='32px';
		 controlText.style.marginTop='0px';
		 controlText.style.marginBottom='0px';
		 controlText.style.marginLeft='0px';
		 controlText.style.marginRight='0px';
		 controlText.style.paddingTop='0px';
		 controlText.style.paddingBottom='0px';
		 controlText.style.paddingLeft='0px';
		 controlText.style.paddingRight='0px';
		 controlText.style.cssFloat='left';
		  	controlText.innerHTML = '<div align="center"> <img src="css/icons/loc-64.png" width="32" height="32" border="0"/></div>';
		  	controlUI.appendChild(controlText);

		  	// Setup the click event listeners: simply set the map to
		  	// Chicago
		  	google.maps.event.addDomListener(controlUI, 'click', function() {
			    //map.setCenter(chicago);
			    //alert('Home');
			    THIS.Get_Ubicacion();
			    map.setZoom(THIS.defZoom + 1); 
		  	});
		};

		this.RestControl= function(controlDiv, map) {

			// Set CSS styles for the DIV containing the control
		  	// Setting padding to 5 px will offset the control
		  	// from the edge of the map
		  	controlDiv.style.padding = '5px';

		  	// Set CSS for the control border
		  	var controlUI = document.createElement('div');
		  	controlUI.style.backgroundColor = 'white';
		  	controlUI.style.borderStyle = 'solid';
		  	controlUI.style.borderWidth = '2px';
		  	controlUI.style.cursor = 'pointer';
		  	controlUI.style.textAlign = 'center';
		  	controlUI.title = 'Click to set the map to Home';
		  	controlDiv.appendChild(controlUI);

		  	// Set CSS for the control interior
		  	var controlText = document.createElement('div');
		  	controlText.style.fontFamily = 'Arial,sans-serif';
		  	controlText.style.fontSize = '12px';
		  	controlText.style.paddingLeft = '4px';
		  	controlText.style.paddingRight = '4px';
		  	controlText.innerHTML = '<b>Restaurant</b>';
		  	controlUI.appendChild(controlText);

		  	// Setup the click event listeners: simply set the map to
		  	// Chicago
		  	google.maps.event.addDomListener(controlUI, 'click', function() {
			    map.setCenter(RestCoors);
			    map.setZoom(this.defZoom); 
			    //alert('Home');
		  	});
		};

		this.RutaControl= function(controlDiv, map) {

			// Set CSS styles for the DIV containing the control
		  	// Setting padding to 5 px will offset the control
		  	// from the edge of the map
		  	controlDiv.style.padding = '5px';

		  	// Set CSS for the control border
		  	var controlUI = document.createElement('div');
		  	controlUI.style.backgroundColor = 'white';
		  	controlUI.style.borderStyle = 'solid';
		  	controlUI.style.borderWidth = '2px';
		  	controlUI.style.cursor = 'pointer';
		  	controlUI.style.textAlign = 'center';
		  	controlUI.title = 'Click to set the map to Home';
		  	controlDiv.appendChild(controlUI);

		  	// Set CSS for the control interior
		  	var controlText = document.createElement('div');
		  	controlText.style.fontFamily = 'Arial,sans-serif';
		  	controlText.style.fontSize = '12px';
		  	controlText.style.paddingLeft = '4px';
		  	controlText.style.paddingRight = '4px';
		  	controlText.innerHTML = '<b>Ruta</b>';
		  	controlUI.appendChild(controlText);

		  	// Setup the click event listeners: simply set the map to
		  	// Chicago
		  	google.maps.event.addDomListener(controlUI, 'click', function() {

			    this.Get_Ruta();
			    this.deleteMarkers();
			    //AAA();
		  	});
		};

		this.loadMarkers= function(){
			
			var THIS = this;
			
			$.getJSON('js/dTiendas.js', function (data) {
 
        		//alert(data);
 
        		$.each(data, function (index, value) {
        			//<li><img src="img/1.png" alt="Landscape 5" class="enfocada"></li>
        			
        			THIS.addMarker(new google.maps.LatLng(value.xx, value.yy), '7-Eleven', 1, false);
        			console.log('Agregando... ' + index);
                	
        		});
     		});
			
		};

		this.loadRemoteMarkers = function(){
			
			var THIS = this;
			
			$.post(ref.F_serv, {
			    sessionid:"0000",
			    request_type:"s_store"
			  },
			  function(data,status){
        		
        		$.each(data.stores, function (index, value) {
        			//<li><img src="img/1.png" alt="Landscape 5" class="enfocada"></li>
        			
        			THIS.addMarker(new google.maps.LatLng(value.xx, value.yy), '7-Eleven', 1, false, value.id, value.name);
        			console.log('Agregando... ' + index);
                	
        		});
			    
			    
			  }, 'json'); 			
  			
			
		};
		
		this.loadMarkersDATA = function(DATA){
			
			var THIS = this;
        		
    		$.each(DATA, function (index, value) {
    			//<li><img src="img/1.png" alt="Landscape 5" class="enfocada"></li>
    			
    			THIS.addMarker(new google.maps.LatLng(value.xx, value.yy), '7-Eleven', 1, true, value.id, value.name);
    			console.log('Agregando... ' + index);
            	
    		});
  			
			
		};


	  	this.handleNoGeolocation= function(errorFlag) {
	  		if (errorFlag) {
		    	var content = 'Error: The Geolocation service failed.';
		  	} else {
			    var content = 'Error: Your browser doesn\'t support geolocation.';
		  	}

		  	var options = {
			    map: map,
			    position: new google.maps.LatLng(60, 105),
			    content: content
		  	};

		  	var infowindow = new google.maps.InfoWindow(options);			  
			  
		  	//map.setCenter(options.position);
		};		

		this.deleteMarkers= function () {
	  		if(this.markers.length > 0){
	  			this.setAllMap(null);  			
				this.markers = [];
			}
		};

		this.SHOW = function(){
			$('#' + this.Id_Element).show();
		};
		
		this.HIDE = function(){
			$('#' + this.Id_Element).hide();
		};

		this.addMarker= function (Position, Title, Type, Save, _ID, _Name, _ItemsList, _ItemsLeft) {
			var marker = new google.maps.Marker({
				position: Position, 
				map: this.map,
				title: Title,
				animation: google.maps.Animation.DROP,
				icon : this.icons[Type],
				
				//zIndexProcess: Math.abs((Position.lng() - parseInt(Position.lng()))*1000),
				
				id: _ID,
				name: _Name,
				itemsLeft: _ItemsLeft
			});
			
			//console.log(Math.abs((Position.lng() - parseInt(Position.lng()))*1000));

			if(Save){
				this.markers.push(marker);
			}			
			
			var pop_up_info = "border: 0px solid black; background-color: #ffffff; padding:15px; margin-top: 8px; border-radius:10px; -moz-border-radius: 10px; -webkit-border-radius: 10px; box-shadow: 1px 1px #888;";
			
			var infoboxOptionsGlastonbury;
			infoboxGlastonbury = new InfoBox();
			 
			//Add an 'event listener' to the Glastonbury map marker to listen out for when it is clicked.
			google.maps.event.addListener(marker, "click", function (e) {
				
				if(_ItemsList != null){					
					
					var boxTextGlastonbury = document.createElement("div");
					boxTextGlastonbury.style.cssText = pop_up_info;
					boxTextGlastonbury.innerHTML = '<span class="pop_up_box_text">'+_ItemsList+'</span>';
					
					infoboxOptionsGlastonbury = {
					 content: boxTextGlastonbury
					 ,disableAutoPan: false
					 ,maxWidth: 0
					 ,pixelOffset: new google.maps.Size(-120, 0)
					 ,zIndex: null
					 ,boxStyle: {
					 background: "url('infobox/pop_up_box_top_arrow.png') no-repeat"
					 ,opacity: 1
					 ,width: "230px"
					 }
					 ,closeBoxMargin: "10px 2px 2px 2px"
					 ,closeBoxURL: "icons/button_close.png"
					 ,infoBoxClearance: new google.maps.Size(1, 1)
					 ,isHidden: false
					 ,pane: "floatPane"
					 ,enableEventPropagation: false
					};
					 
					infoboxGlastonbury.setOptions(infoboxOptionsGlastonbury);										
					
				 infoboxGlastonbury.open(this.map, this);
				 this.setZIndex(google.maps.Marker.MAX_ZINDEX + 1);
				 this.map.setCenter(marker.getPosition());
					
				}
								
			});	
		
		};

		this.setAllMap= function(map) {
	  		for (var i = 0; i < this.markers.length; i++) {
    		this.markers[i].setMap(map);
  			}
		};

		this.Get_Ubicacion= function() {
			
			var THIS = this;

			var pos = null;

		  	if(navigator.geolocation) {
	    		navigator.geolocation.getCurrentPosition(function(position) {
	    		
	    		//alert('__ ' + MiUbicaCoors);

	      		pos = new google.maps.LatLng(position.coords.latitude,
	                                       position.coords.longitude);
	                                       
               MiPos = pos;

				//alert('__ ' + MiUbicaCoors);
	      		THIS.map.setCenter(pos);
	      		
	    	}, function() {
	      		handleNoGeolocation(true);      		

	    	});
  			} else {
	    			// Browser doesn't support Geolocation
	    		handleNoGeolocation(false);
	  		}

	  		//return pos;
		};

		this.i_PuntoA= function() {

			var pos = null;
			
			var THIS = this;
			
			console.log('AAAAAAAA');

		  	if(navigator.geolocation) {
	    		navigator.geolocation.getCurrentPosition(function(position) {

	      		pos = new google.maps.LatLng(position.coords.latitude,
	                                       position.coords.longitude);
	      		
	      		THIS.DataLocation(THIS.markers[0].position.lat(), THIS.markers[0].position.lng(), '#'+THIS.Id_Element + '-data');


	      		var start = pos;
			  	var end = THIS.markers[0].position;//new google.maps.LatLng(25.753285850030707, -100.29801327735186);//THIS.markers[0];
			  	var request = {
			      	origin:start,
			      	destination:end,
			      	travelMode: google.maps.TravelMode.DRIVING
			  	};
			  	THIS.directionsService.route(request, function(response, status) {
				    if (status == google.maps.DirectionsStatus.OK) {
			      	THIS.directionsDisplay.setDirections(response);
			    	}
			  	});
	      		
	    	}, function() {
	      		handleNoGeolocation(true);      		

	    	});
  			} else {
	    			// Browser doesn't support Geolocation
	    		handleNoGeolocation(false);
	  		}
		};

		this.Get_Ruta= function(){

			if(this.markers.length <= 0 && this.MiUbica == false)
			{
				//alert('INI');
				this.MiUbica = true;
				this.i_PuntoA();

			}
			else{

			  	var start = this.markers[0].position;
			  	var end = RestCoors;
			  	var request = {
			      	origin:start,
			      	destination:end,
			      	travelMode: google.maps.TravelMode.DRIVING
			  	};
			  	this.directionsService.route(request, function(response, status) {
				    if (status == google.maps.DirectionsStatus.OK) {
			      	directionsDisplay.setDirections(response);
			    	}
			  	});
		  	}
		};

		this.DataLocation= function (xx, yy, UpdateIn) {

			var DIR = {Numero: 0,
				Calle: 'NO',
				Colonia: 'NO',
				Municipio: 'NO',
				Estado: 'NO',
				Pais: 'NO',
				CP: 0};
		
			$.post('http://maps.googleapis.com/maps/api/geocode/json?latlng=' + xx + ', ' + yy + '&sensor=true', 
			{

			}, function(datos){

				for(var i=0; i<datos.results[0].address_components.length; i++)
				{
					if(datos.results[0].address_components[i].types[0] == 'street_number')
						DIR.Numero = datos.results[0].address_components[i].long_name;					

					if(datos.results[0].address_components[i].types[0] == "route")
						DIR.Calle = datos.results[0].address_components[i].long_name;
					

					if(datos.results[0].address_components[i].types[0] == "neighborhood")
						DIR.Colonia = datos.results[0].address_components[i].long_name;
					

					if(datos.results[0].address_components[i].types[0] == "administrative_area_level_2" ||
						datos.results[0].address_components[i].types[0] == "locality")
					{
						DIR.Municipio = datos.results[0].address_components[i].long_name;
					}

					if(datos.results[0].address_components[i].types[0] == "administrative_area_level_1")
					{
						DIR.Estado = datos.results[0].address_components[i].long_name;
						DIR.Estado_2 = datos.results[0].address_components[i].short_name;
					}	

					if(datos.results[0].address_components[i].types[0] == "country")
					{
						DIR.Pais= datos.results[0].address_components[i].long_name;
						DIR.Pais_2= datos.results[0].address_components[i].short_name;
					}

					if(datos.results[0].address_components[i].types[0] == "postal_code")
						DIR.CP= datos.results[0].address_components[i].long_name;

				}


				if(UpdateIn != null && UpdateIn != ''){
					
					$(UpdateIn + ' .L1').text(DIR.Calle + ' #' + DIR.Numero  + ',');
					$(UpdateIn + ' .L2').text('Col. ' + DIR.Colonia + ',');
					//$(UpdateIn + ' .L3').text(DIR.Estado_2 + ', ' + DIR.Pais_2);
					$(UpdateIn + ' .L3').text(DIR.Municipio + ', ' + DIR.Estado_2 + ', ' + DIR.Pais_2);
				}

				/*alert(DIR.Calle + ' #' + DIR.Numero + ', ' + DIR.Colonia + ', ' + DIR.Municipio + ', ' + 
					DIR.Estado + ', ' + DIR.Pais + ', ' + DIR.CP);*/			
			
			}).error(function(){
				alert('error... ohh no!');
			});

			return DIR;
		};
		
		this.clearDataLoc= function(){
			$('#'+this.Id_Element + '-data .L1').text('');
			$('#'+this.Id_Element + '-data .L2').text('');
			$('#'+this.Id_Element + '-data .L3').text('');
			
		};
		
		this.getPathTo= function(Sucursal) {
			
			var THIS = this;
			
			
			console.log('BEST PATH');
			
	      		
      		this.DataLocation(Sucursal.xx, Sucursal.yy, '#'+this.Id_Element + '-data');


      		var start = MiPos;
		  	var end = new google.maps.LatLng(Sucursal.xx, Sucursal.yy);//THIS.markers[0];
		  	var request = {
		      	origin:start,
		      	destination:end,
		      	travelMode: google.maps.TravelMode.DRIVING
		  	};
		  	this.directionsService.route(request, function(response, status) {
			    if (status == google.maps.DirectionsStatus.OK) {
		      		THIS.directionsDisplay.setDirections(response);
		      		//console.log('HIIIIIIIIIIIIIIIII');
		      		
		      		setTimeout(function(){
		      			try{
		      				//console.log('Hoooooooooooooooooo');
		      				var indx = THIS.arrayObjectIndexOf(THIS.markers, Sucursal.idTienda, 'id');
		      				google.maps.event.trigger(THIS.markers[indx], 'click');				
						}
						catch(ERR){
							console.log('ERROR: NOT FROM LIST --' + ERR);
						}
    				},0); 
		      		
		      		
		      		
		      	
		    	}
		    	
		    	
		  	});
		  	
		  	
		  	
		
		};
		
		this.getNearSuc = function(SucPoscns, ItemsList){
			
			var Distancias = [];
			var THIS = this;
			
			var CloserSuc = {obj: null, dist: 100000000, itemsLeft: 100000000};
			
			$.each(SucPoscns, function (index, value) {
				//console.log(MiPos.lat() + ' ?? ' + value.xx);
				Distancias.push(THIS.getDistance(MiPos, value));
				//console.log(Distancias[index] + ' Distance ' + value.idTienda);
				
				
				
				
				if(ItemsList){//SÍ, Importa la prioriadad de la lista.
					var indx = THIS.arrayObjectIndexOf(app.idTienda_ItemsLeft, value.idTienda, 'idTienda');
					//console.log('value.itemsLeft: IF ' + app.idTienda_ItemsLeft[indx].itemsLeft + ' < ' + CloserSuc.itemsLeft);
					if(app.idTienda_ItemsLeft[indx].itemsLeft < CloserSuc.itemsLeft){
							CloserSuc.obj = value;
							CloserSuc.dist = Distancias[index];
							CloserSuc.itemsLeft = app.idTienda_ItemsLeft[indx].itemsLeft;											
					}
					else if(app.idTienda_ItemsLeft[indx].itemsLeft == CloserSuc.itemsLeft){
						if(Distancias[index] < CloserSuc.dist){
							console.log('IF ' + Distancias[index]  + ' < ' + CloserSuc.dist);
							console.log('IF ' + app.idTienda_ItemsLeft[indx].itemsLeft + ' <= ' + CloserSuc.itemsLeft);
							CloserSuc.obj = value;
							CloserSuc.dist = Distancias[index];
							CloserSuc.itemsLeft = app.idTienda_ItemsLeft[indx].itemsLeft;
						}					
					}					
				}
				else{//Solo importa la distancia.
					if(Distancias[index] < CloserSuc.dist){
						CloserSuc.obj = value;
						CloserSuc.dist = Distancias[index];
					}
				}
				
				
			});
			
			this.getPathTo(CloserSuc.obj);
			this.BestSuc = CloserSuc.obj;
			
		};
		
		this.arrayObjectIndexOf = function (myArray, searchTerm, property) {
		    for(var i = 0, len = myArray.length; i < len; i++) {
		        if (myArray[i][property] === searchTerm) return i;
		    }
		    return -1;
		};
		
		this.rad = function(x) {
		  	return x * Math.PI / 180;
		};
	
		this.getDistance = function(p1, p2) {
			var R = 6378137; // el radio de la tierra
		  	var dLat = this.rad(p2.xx - p1.lat());
		  	var dLong = this.rad(p2.yy - p1.lng());
		  	var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
			    Math.cos(this.rad(p1.lat())) * Math.cos(this.rad(p2.xx)) *
		    	Math.sin(dLong / 2) * Math.sin(dLong / 2);
		  	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		  	var d = R * c;
		  	return d; // distancia en metros
		};
		
	};
 
 	var DB ={
		
		deleteMarkers: new Array(),
		
		allStores: new Array(),
		
		idStoresWithP: [],
		
		
		s_AllStores : function(MapToAddMarkers, nICON){
			
			$.mobile.loading( 'show', {
				text: 'Cargando Sucursales',
				textVisible: true,
				theme: 'a',
				html: ""
			});
			
			MapToAddMarkers.HIDE();
			
			var DATA;
			
			$.post(ref.F_serv, {
			    sessionid:"0000",
			    request_type:"s_store"
			  },
			  function(data,status){
			  	
			  	DATA = data.stores;
			  	allStores = data.stores;
			  	
			  	MapToAddMarkers.initialize(DATA, nICON);	    
			    
			  }, 'json').done(function(){
			  	
			  	$.mobile.loading('hide');
			  	
			  	MapToAddMarkers.SHOW();
			  });
			
					
		},
		
		
	};
	
  	
});